import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {  
    return Scaffold(
      appBar: AppBar(
        title: const Text("My portfolio"),
      ),
      body: Container(
        child: Column(
          children: [
            
            Container(
              height: 100.0,
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: const [
                  
                Text("Alleene Lacaba", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
            
              ]),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                SizedBox(
                  height: 300.0,
                  child: Column(children: [
                    Text("About Me", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),),
                    Text("I build websites and mobile apps", style: TextStyle(fontSize: 12.0),),
                    TextButton(
                      onPressed: (){
                        _launchUrl();
                      }, child: const Text("Check out my Profile"))
                  
                
                  ],),
                ),
                // Contains the profile
                Image.asset(
                  'assets/images/my_photo.jpg',
                  height: 300,
                  width: 300,
                ), // Image.asset
              ]),
            ),
          ],
        ),
      )
    );
  }

  Future<void> _launchUrl() async {
    final Uri _url = Uri.parse('https://www.instagram.com/_trc_team/?next=%2F');
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }

  
}
